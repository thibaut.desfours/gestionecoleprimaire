package com.gep.beans;

public class Prof extends Personne {
	private Classe classe;
	public Prof(int id, String nom, String prenom, String adresse, String mail, String tel, String ville, Classe classe) {
		super(id, nom, prenom, adresse, mail, tel, ville);
		this.classe = classe;
	}
	
	
	public Classe getClasse() {
		return classe;
	}


	public void setClasse(Classe classe) {
		this.classe = classe;
	}


	public Prof() {
		super();
	}
}
