package com.gep.beans;

public class Niveau {
	private int id;
	private String libelle;
	
	public int getId() {
		return id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Niveau(int id, String libelle) {
		super();
		this.id = id;
		this.libelle = libelle;
	}
	
	public Niveau() {
	
	}
	
}
