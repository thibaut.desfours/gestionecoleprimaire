package com.gep.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MatiereRepository {
	private Connection bdd;
	
	public Connection getBdd() {
		return bdd;
	}

	public void setBdd(Connection bdd) {
		this.bdd = bdd;
	}

	public MatiereRepository() {
		super();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.bdd = ConnexionBDD.connexion();
	}
	
	public ArrayList<Matiere> getAll(){
		ArrayList<Matiere> matieres = new ArrayList();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from matiere;");
			ResultSet rs = statement.executeQuery();
	        while(rs.next())
	        	matieres.add(new Matiere(rs.getInt("matiere_id"),rs.getString("libelle")));
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return matieres;
	}
	
	public Matiere getMatiereById(int id){
		Matiere matiere = null;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from matiere where matiere_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	matiere = new Matiere(rs.getInt("matiere_id"),rs.getString("libelle"));
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return matiere;
	}
	
	
}
