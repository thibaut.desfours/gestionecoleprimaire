package com.gep.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProfRepository {
	private Connection bdd;
		
		public ProfRepository() {
			super();
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.bdd = ConnexionBDD.connexion();
		}

		public Connection getBdd() {
			return bdd;
		}

		public void setBdd(Connection bdd) {
			this.bdd = bdd;
		}
		
		public int countAll() {
			int result = 0;
			try {
				PreparedStatement statement = this.getBdd().prepareStatement("select count(*) as nb from prof");
				ResultSet rs = statement.executeQuery();
		        if(rs.next())
		        	result = rs.getInt("nb");
			}
			catch(SQLException e) {
				System.out.println("SQLException: " + e.getMessage());
	            System.out.println("SQLState: " + e.getSQLState());
	            System.out.println("VendorError: " + e.getErrorCode());
			}
			return result;
		}
		
		public ArrayList<Prof> getAll(){
			ArrayList<Prof> profs = new ArrayList();

			ClasseRepository classeR = new ClasseRepository();
			try {
				PreparedStatement statement = this.getBdd().prepareStatement("select * from prof");
				ResultSet rs = statement.executeQuery();
		        while(rs.next())
		        	profs.add(new Prof(rs.getInt("prof_id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("adresse"),rs.getString("mail"),rs.getString("tel"),rs.getString("ville"),classeR.getClasseByProfId(rs.getInt("prof_id"))));
			}
			catch(SQLException e) {
				System.out.println("SQLException: " + e.getMessage());
	            System.out.println("SQLState: " + e.getSQLState());
	            System.out.println("VendorError: " + e.getErrorCode());
			}	
			return profs;
		}
		
		public ArrayList<Prof> getAllWithoutClass(){
			ArrayList<Prof> profs = new ArrayList();

			ClasseRepository classeR = new ClasseRepository();
			try {
				PreparedStatement statement = this.getBdd().prepareStatement("select * from prof where not exists(select 1 from classe where classe.classe_id = prof.prof_id);");
				ResultSet rs = statement.executeQuery();
		        while(rs.next())
		        	profs.add(new Prof(rs.getInt("prof_id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("adresse"),rs.getString("mail"),rs.getString("tel"),rs.getString("ville"),classeR.getClasseByProfId(rs.getInt("prof_id"))));
			}
			catch(SQLException e) {
				System.out.println("SQLException: " + e.getMessage());
	            System.out.println("SQLState: " + e.getSQLState());
	            System.out.println("VendorError: " + e.getErrorCode());
			}	
			return profs;
		}
		
		public Prof getById(int id) {
			Prof prof = null;
			ClasseRepository classeR = new ClasseRepository();
			
			try {
				PreparedStatement statement = this.getBdd().prepareStatement("select * from prof where prof_id = ?;");
				statement.setInt(1,id);
				ResultSet rs = statement.executeQuery();
		        if(rs.next())
		        	prof = new Prof(rs.getInt("prof_id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("adresse"),rs.getString("mail"),rs.getString("tel"),rs.getString("ville"),classeR.getClasseByProfId(rs.getInt("prof_id")));
			}
			catch(SQLException e) {
				System.out.println("SQLException: " + e.getMessage());
	            System.out.println("SQLState: " + e.getSQLState());
	            System.out.println("VendorError: " + e.getErrorCode());
			}	
			return prof;
		}
		
		public boolean updateProf(Prof prof) {
			try {
				PreparedStatement statement = this.getBdd().prepareStatement("update prof set nom = ?, prenom = ?, tel = ?, mail = ?, adresse = ?, ville = ? where prof_id = ?;");
				statement.setString(1, prof.getNom());
				statement.setString(2,prof.getPrenom());
				statement.setString(3,prof.getTel());
				statement.setString(4,prof.getMail());
				statement.setString(5,prof.getAdresse());
				statement.setString(6,prof.getVille());
				statement.setInt(7,prof.getId());
				
				if(statement.executeUpdate() < 1)
					return false;
		
			} catch (SQLException ex) {
	        	System.out.println("SQLException: " + ex.getMessage());
	            System.out.println("SQLState: " + ex.getSQLState());
	            System.out.println("VendorError: " + ex.getErrorCode());
	        }
			return true;
		}
		
		public boolean insertProf(Prof prof) {
			try {
				PreparedStatement statement = this.getBdd().prepareStatement("insert into prof values (null,?,?,?,?,?,?);");
				statement.setString(1, prof.getNom());
				statement.setString(2,prof.getPrenom());
				statement.setString(3,prof.getTel());
				statement.setString(4,prof.getMail());
				statement.setString(5,prof.getAdresse());
				statement.setString(6,prof.getVille());
				
				if(statement.executeUpdate() < 1)
					return false;
		
			} catch (SQLException ex) {
	        	System.out.println("SQLException: " + ex.getMessage());
	            System.out.println("SQLState: " + ex.getSQLState());
	            System.out.println("VendorError: " + ex.getErrorCode());
	        }
			return true;
		}
		
		public boolean deleteProf(int id) {
			try {
				PreparedStatement statement2 = this.getBdd().prepareStatement("update classe set prof_id = null where prof_id = ?");
				statement2.setInt(1, id);
				statement2.executeUpdate();
				
				PreparedStatement statement = this.getBdd().prepareStatement("delete from prof where prof_id = ?;");
				statement.setInt(1, id);
				
				if(statement.executeUpdate() < 1)
					return false;			
			} catch (SQLException ex) {
	        	System.out.println("SQLException: " + ex.getMessage());
	            System.out.println("SQLState: " + ex.getSQLState());
	            System.out.println("VendorError: " + ex.getErrorCode());
	        }
			return true;
		}
		
		
}
