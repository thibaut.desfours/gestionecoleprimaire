package com.gep.beans;

public class Evaluation {
	private int id;
	private String intitule;
	private Cours cours;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIntitule() {
		return intitule;
	}
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	public Cours getCours() {
		return cours;
	}
	public void setCours(Cours cours) {
		this.cours = cours;
	}
	public Evaluation(int id, String intitule, Cours cours) {
		super();
		this.id = id;
		this.intitule = intitule;
		this.cours = cours;
	}
	
	
}
