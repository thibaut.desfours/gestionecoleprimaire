package com.gep.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class EvaluationRepository {
	private Connection bdd;
	
	public EvaluationRepository() {
		super();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.bdd = ConnexionBDD.connexion();
	}
	
	public Connection getBdd() {
		return bdd;
	}

	public void setBdd(Connection bdd) {
		this.bdd = bdd;
	}
	
	public int countAll() {
		int result = 0;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select count(*) as nb from evaluation");
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	result = rs.getInt("nb");
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return result;
	}
	
	public ArrayList<Evaluation> getAll() {
		ArrayList<Evaluation> evaluations = new ArrayList();
		CoursRepository coursR = new CoursRepository();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from evaluation;");
			ResultSet rs = statement.executeQuery();
	       while(rs.next())
	    	   evaluations.add(new Evaluation(rs.getInt("evaluation_id"),rs.getString("intitule"),coursR.getCoursById(rs.getInt("cours_id"))));	   
	    }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return evaluations;
	}	
	
	public Evaluation getEvaluationById(int id) {
		Evaluation evaluation = null;
		CoursRepository coursR = new CoursRepository();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from evaluation where evaluation_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if(rs.next())
	    	   evaluation = new Evaluation(rs.getInt("evaluation_id"),rs.getString("intitule"),coursR.getCoursById(rs.getInt("cours_id")));	   
	    }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return evaluation;
	}
	
	public boolean deleteEvaluation(int id) {
		Evaluation evaluation = null;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("delete from passer where evaluation_id = ?;");
			statement.setInt(1, id);
			statement.executeUpdate();
			
			statement = this.getBdd().prepareStatement("delete from evaluation where evaluation_id = ?;");
			statement.setInt(1, id);
			if(statement.executeUpdate()<1)
				return false;			
	    }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return true;
	}
	
	public boolean updateEvaluation(Evaluation eval) {
		Evaluation evaluation = null;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("update evaluation set intitule = ?, cours_id = ? where evaluation_id = ?;");
			statement.setString(1, eval.getIntitule());
			statement.setInt(2, eval.getCours().getId());
			statement.setInt(3, eval.getId());

			if(statement.executeUpdate()<1)
				return false;			
	    }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return true;
	}
	
	public boolean insertEvaluation(Evaluation eval) {
		EleveRepository eleveR = new EleveRepository();
		Evaluation evaluation = null;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("insert into evaluation values (null,?,?);");
			statement.setString(1, eval.getIntitule());
			statement.setInt(2, eval.getCours().getId());
			if(statement.executeUpdate()<1)
				return false;
			
			ArrayList<Eleve> eleves = eleveR.getAllByClasse(eval.getCours().getClasse().getId());
			for(Eleve eleve : eleves) {
				statement = this.getBdd().prepareStatement("insert into passer values (?,(select max(evaluation_id) from evaluation),-1);");
				statement.setInt(1, eleve.getId());
				System.out.println(eleve.getId()+" / "+eval.getId());
				if(statement.executeUpdate()<1)
					return false;
			}			
	    }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return true;
	}
	
	public HashMap<Integer,Float> getNotesByEvaluation(int id){
		HashMap<Integer,Float> notes = new HashMap();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select eleve_id, note from passer where evaluation_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			while(rs.next())
	    	   notes.put(rs.getInt("eleve_id"), rs.getFloat("note"));
	    }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}		
		return notes;
	}
	
	public boolean updateNote(int eleveId, int evalId, float note) {
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("update passer set note = ? where eleve_id = ? and evaluation_id = ?;");
			statement.setFloat(1, note);
			statement.setInt(2, eleveId);
			statement.setInt(3, evalId);
			if(statement.executeUpdate()<1)
				return false;			
	    }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}		
		return true;
	}
}
