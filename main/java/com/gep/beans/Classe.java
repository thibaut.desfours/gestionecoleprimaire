package com.gep.beans;

import java.util.ArrayList;

public class Classe {
	private int id;
	private String nom;
	private int annee;
	private Prof prof;
	private Niveau niveau;
	private ArrayList<Eleve> eleves;
	
	public ArrayList<Eleve> getEleves() {
		return eleves;
	}
	public void setEleves(ArrayList<Eleve> eleves) {
		this.eleves = eleves;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	public Prof getProf() {
		return prof;
	}
	public void setProf(Prof prof) {
		this.prof = prof;
	}
	public Niveau getNiveau() {
		return niveau;
	}
	public void setNiveau(Niveau niveau) {
		this.niveau = niveau;
	}
	
	public Classe(int id, String nom, int annee, Prof prof, Niveau niveau, ArrayList<Eleve> eleves) {
		super();
		this.id = id;
		this.nom = nom;
		this.annee = annee;
		this.prof = prof;
		this.niveau = niveau;
		this.eleves = eleves;
	}
	
	public Classe() {
		
	}
	
	
}
