package com.gep.beans;

import java.util.Date;

public class Eleve extends Personne{
	private Date dateN;
	private Classe classe;
	
	public Eleve(int id, String nom, String prenom, String adresse, String mail, String tel, String ville, Date dateN,
			Classe classe) {
		super(id, nom, prenom, adresse, mail, tel, ville);
		this.dateN = dateN;
		this.classe = classe;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public Date getDateN() {
		return dateN;
	}

	public void setDateN(Date dateN) {
		this.dateN = dateN;
	}
}
