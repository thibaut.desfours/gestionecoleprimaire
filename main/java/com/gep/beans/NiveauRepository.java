package com.gep.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class NiveauRepository {
	private Connection bdd;
	
	public NiveauRepository() {
		super();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.bdd = ConnexionBDD.connexion();
	}

	public Connection getBdd() {
		return bdd;
	}

	public void setBdd(Connection bdd) {
		this.bdd = bdd;
	}
	
	public ArrayList<Niveau> getAll(){
		ArrayList<Niveau> niveaux = new ArrayList();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from niveau");
			ResultSet rs = statement.executeQuery();
	        while(rs.next())
	        	niveaux.add(new Niveau(rs.getInt("niveau_id"),rs.getString("libelle")));
		 }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return niveaux;
	}
	
	public Niveau getById(int id){
		Niveau niveau = null;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from niveau where niveau_id = ?;");
			statement.setInt(1,id);
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	niveau = new Niveau(rs.getInt("niveau_id"),rs.getString("libelle"));
		 }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return niveau;
	}
}

