package com.gep.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class EleveRepository {
	private Connection bdd;
	
	Format formatter = new SimpleDateFormat("yyyy-mm-dd");
	
	public EleveRepository() {
		super();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.bdd = ConnexionBDD.connexion();
	}
	
	public int countAll() {
		int result = 0;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select count(*) as nb from eleve");
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	result = rs.getInt("nb");
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return result;
	}
	
	public ArrayList<Eleve> getAll() {
		ArrayList<Eleve> eleves = new ArrayList();
		ClasseRepository classeR = new ClasseRepository();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from eleve;");
			ResultSet rs = statement.executeQuery();
	       while(rs.next())
	    	   eleves.add(new Eleve(rs.getInt("eleve_id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("adresseResp"),rs.getString("mailResp"),rs.getString("telResp"),rs.getString("villeResp"),rs.getDate("dateN"),classeR.getClasseByEleveId(rs.getInt("eleve_id"))));	
	    }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return eleves;
	}
	
	public ArrayList<Eleve> getAllWithoutClass() {
		ArrayList<Eleve> eleves = new ArrayList();
		ClasseRepository classeR = new ClasseRepository();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from eleve where not exists(select 1 from appartenir where appartenir.eleve_id = eleve.eleve_id and annee = (select year(sysdate()) from dual));");
			ResultSet rs = statement.executeQuery();
	       while(rs.next())
	        	eleves.add(new Eleve(rs.getInt("eleve_id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("adresseResp"),rs.getString("mailResp"),rs.getString("telResp"),rs.getString("villeResp"),rs.getDate("dateN"),null));
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return eleves;
	}
	
	public ArrayList<Eleve> getAllByClasse(int classId) {
		ArrayList<Eleve> eleves = new ArrayList();
		ClasseRepository classeR = new ClasseRepository();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from eleve join appartenir using(eleve_id)  where classe_id = ? and annee = (select year(sysdate()) from dual)");
			statement.setInt(1, classId);
			ResultSet rs = statement.executeQuery();
	       while(rs.next())
	        	eleves.add(new Eleve(rs.getInt("eleve_id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("adresseResp"),rs.getString("mailResp"),rs.getString("telResp"),rs.getString("villeResp"),rs.getDate("dateN"),classeR.getClasseByEleveId(rs.getInt("eleve_id"))));
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return eleves;
	}
	
	public ArrayList<Eleve> getAllByEvaluation(int id) {
		ArrayList<Eleve> eleves = new ArrayList();
		ClasseRepository classeR = new ClasseRepository();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select eleve.* from eleve join passer using(eleve_id) where evaluation_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
	       while(rs.next())
	        	eleves.add(new Eleve(rs.getInt("eleve_id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("adresseResp"),rs.getString("mailResp"),rs.getString("telResp"),rs.getString("villeResp"),rs.getDate("dateN"),classeR.getClasseByEleveId(rs.getInt("eleve_id"))));
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return eleves;
	}
	
	public Eleve getById(int id) {
		ClasseRepository classeR = new ClasseRepository();
		Eleve eleve = null;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from eleve where eleve_id = ?");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if(rs.next())
				eleve = new Eleve(rs.getInt("eleve_id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("adresseResp"),rs.getString("mailResp"),rs.getString("telResp"),rs.getString("villeResp"),rs.getDate("dateN"),classeR.getClasseByEleveId(rs.getInt("eleve_id")));	
		} catch (SQLException ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		return eleve;
	}
	
	public boolean updateEleve(Eleve eleve) {
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("update eleve set nom = ?, prenom = ?, dateN = ?, telResp = ?, mailResp = ?, adresseResp = ?, villeResp = ? where eleve_id = ?;");
			statement.setString(1, eleve.getNom());
			statement.setString(2, eleve.getPrenom());
			statement.setString(3, formatter.format(eleve.getDateN()));
			statement.setString(4, eleve.getTel());
			statement.setString(5, eleve.getMail());
			statement.setString(6, eleve.getAdresse());
			statement.setString(7, eleve.getVille());
			statement.setInt(8, eleve.getId());
			
			if(statement.executeUpdate() < 1)
				return false;
	
			PreparedStatement statement2 = this.getBdd().prepareStatement("update appartenir set classe_id = ?, annee = (select year(sysdate()) from dual) where eleve_id = ?;");
			statement2.setInt(1,eleve.getClasse().getId());
			statement2.setInt(2, eleve.getId());
			if(statement2.executeUpdate() < 1)
				return false;
				
			
		} catch (SQLException ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		return true;
	}
	
	public boolean insertEleve(Eleve eleve) {
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("insert into eleve values(null,?,?,?,?,?,?,?);");
			statement.setString(1, eleve.getNom());
			statement.setString(2, eleve.getPrenom());
			statement.setString(3, formatter.format(eleve.getDateN()));
			statement.setString(4, eleve.getAdresse());
			statement.setString(5, eleve.getTel());
			statement.setString(6, eleve.getVille());
			statement.setString(7, eleve.getMail());
			
			if(statement.executeUpdate() < 1)
				return false;		
			PreparedStatement statement2 = this.getBdd().prepareStatement("insert into appartenir values((select max(eleve_id) from eleve),?,(select year(sysdate()) from dual));");
			statement2.setInt(1, eleve.getClasse().getId());
			
			if(statement2.executeUpdate() < 1)
				return false;
		} catch (SQLException ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		return true;
	}
	
	public boolean deleteEleve(int id) {
		try {
			PreparedStatement statement2 = this.getBdd().prepareStatement("delete from appartenir where eleve_id = ?;");
			statement2.setInt(1, id);
			statement2.executeUpdate();
			
			PreparedStatement statement = this.getBdd().prepareStatement("delete from eleve where eleve_id = ?;");
			statement.setInt(1, id);
			
			if(statement.executeUpdate() < 1)
				return false;			
		} catch (SQLException ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		return true;
	}
	
	public boolean addClass(ArrayList<Integer> eleves, int classeId) {
		try {
			for(int eleveId : eleves) {
				PreparedStatement statement = this.getBdd().prepareStatement("insert into appartenir values (?,?,(select year(sysdate()) from dual));");
				statement.setInt(1,eleveId);
				statement.setInt(2,classeId);				
				statement.executeUpdate();
			}	       
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return true;
	}

	public Connection getBdd() {
		return bdd;
	}

	public void setBdd(Connection bdd) {
		this.bdd = bdd;
	}
	
	



	
}
