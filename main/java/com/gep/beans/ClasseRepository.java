package com.gep.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class ClasseRepository {
	private Connection bdd;
	
	public ClasseRepository() {
		super();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.bdd = ConnexionBDD.connexion();
	}

	public Connection getBdd() {
		return bdd;
	}

	public void setBdd(Connection bdd) {
		this.bdd = bdd;
	}
	
	public int countAll() {
		int result = 0;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select count(*) as nb from classe");
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	result = rs.getInt("nb");
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return result;
	}
	
	public Classe getClasseByEleveId(int eleveId) {
		Classe classe = new Classe();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select classe.* from eleve" + 
					"	join appartenir using(eleve_id) " + 
					"	join classe using(classe_id) " + 
					"	where " + 
					"	appartenir.annee = (select year(sysdate()) from dual) " + 
					"	and classe.annee = appartenir.annee " + 
					"	and eleve.eleve_id = ?; ");
			statement.setInt(1,eleveId);
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	classe = new Classe(rs.getInt("classe_id"),rs.getString("nom"),rs.getInt("annee"),new Prof(),new Niveau(),null);
	        	
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}		
		return classe;
	}
	
	public Classe getClasseByProfId(int profId) {
		Classe classe = null;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select classe.* from prof" + 
					"	join classe using(prof_id) " + 
					"	where " + 
					" prof.prof_id = ?; ");
			statement.setInt(1,profId);
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	classe = new Classe(rs.getInt("classe_id"),rs.getString("nom"),rs.getInt("annee"),new Prof(),new Niveau(),null);
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}		
		return classe;
	}
	
	public Classe getClasseByCoursId(int coursId) {
		Classe classe = null;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select classe.* from classe" + 
					"	join inscrire using(classe_id) " + 
					"	where " + 
					" cours_id = ?; ");
			statement.setInt(1,coursId);
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	classe = new Classe(rs.getInt("classe_id"),rs.getString("nom"),rs.getInt("annee"),new Prof(),new Niveau(),null);
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}		
		return classe;
	}
	
	public Classe getClasseById(int id) {
		Classe classe = null;
		ArrayList <Eleve> eleves = new ArrayList();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement(""
					+ "select classe_id, classe.nom as classe_nom, annee, prof.prof_id, prof.nom as prof_nom, prof.prenom, prof.tel,"
					+ " prof.mail, prof.adresse, prof.ville, niveau.niveau_id, niveau.libelle "
					+ "from classe "
					+ "left join prof on prof.prof_id = classe.prof_id "
					+ "join niveau on niveau.niveau_id = classe.niveau_id "
					+ "where classe.classe_id = ?;");
			statement.setInt(1,id);
			ResultSet rs = statement.executeQuery();
	        if(rs.next()) {
	        	PreparedStatement statement2 = this.getBdd().prepareStatement("select * from eleve join appartenir using(eleve_id)  where classe_id = ? and annee = (select year(sysdate()) from dual);");
				statement2.setInt(1, id);
				ResultSet rs2 = statement2.executeQuery();
		       while(rs2.next())
		        	eleves.add(new Eleve(rs2.getInt("eleve_id"),rs2.getString("nom"),rs2.getString("prenom"),rs2.getString("adresseResp"),rs2.getString("mailResp"),rs2.getString("telResp"),rs2.getString("villeResp"),rs2.getDate("dateN"),this.getClasseByEleveId(rs2.getInt("eleve_id"))));
	        	classe = new Classe(rs.getInt("classe_id"),rs.getString("classe_nom"),rs.getInt("annee"),
	        			new Prof(rs.getInt("prof_id"),rs.getString("prof_nom"),rs.getString("prenom"),rs.getString("adresse"),rs.getString("mail"),rs.getString("tel"),rs.getString("ville"),null),
	        			new Niveau(rs.getInt("niveau_id"),rs.getString("libelle")),eleves);
	        }
	      }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}		
		return classe;
	}
	
	public ArrayList<Classe> getAll() {
		ArrayList<Classe> classes = new ArrayList();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement(""
					+ "select classe_id, classe.nom as classe_nom, annee, prof.prof_id, prof.nom as prof_nom, prof.prenom, prof.tel,"
					+ " prof.mail, prof.adresse, prof.ville, niveau.niveau_id, niveau.libelle "
					+ "from classe "
					+ "left join prof on prof.prof_id = classe.prof_id "
					+ "join niveau on niveau.niveau_id = classe.niveau_id;");
			ResultSet rs = statement.executeQuery();
	        while(rs.next()) {
	        	if(rs.getString("prof_nom") != "")     	
	        		classes.add(new Classe(rs.getInt("classe_id"),rs.getString("classe_nom"),rs.getInt("annee"),
	        			new Prof(rs.getInt("prof_id"),rs.getString("prof_nom"),rs.getString("prenom"),rs.getString("adresse"),rs.getString("mail"),rs.getString("tel"),rs.getString("ville"),null),
	        			new Niveau(rs.getInt("niveau_id"),rs.getString("libelle")),null));
	        	else
	        		classes.add(new Classe(rs.getInt("classe_id"),rs.getString("classe_nom"),rs.getInt("annee"),
		        			null,
		        			new Niveau(rs.getInt("niveau_id"),rs.getString("libelle")),null));
	        }
	    }
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}		
		return classes;
	}
	
	public boolean insertClasse(Classe classe) {
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("insert into classe values(null,?,(select year(sysdate()) from dual),?,?);");
			statement.setString(1,classe.getNom());
			statement.setInt(2,classe.getProf().getId());
			statement.setInt(3,classe.getNiveau().getId());
			
			if(statement.executeUpdate() < 1)
				return false;
		} catch (SQLException ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		return true;
	}
	
	public boolean updateClasse(Classe classe) {
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("update classe set nom = ?, annee = ?, niveau_id = ?, prof_id = ? where classe_id = ?;");
			statement.setString(1, classe.getNom());
			statement.setInt(2, classe.getAnnee());
			statement.setInt(3, classe.getNiveau().getId());
			statement.setInt(4, classe.getProf().getId());
			statement.setInt(5, classe.getId());
			
			if(statement.executeUpdate() < 1)
				return false;
	
		} catch (SQLException ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		return true;
	}
	
	public boolean retirerEleve(int eleveId, int classeId) {
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("delete from appartenir where eleve_id = ? and classe_id = ? and annee = (select year(sysdate()) from dual);");
			statement.setInt(1, eleveId);
			statement.setInt(2,classeId);
			statement.executeUpdate();
		} catch (SQLException ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		return true;
	}
	
	public boolean deleteClasse(int id) {
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("delete from appartenir where classe_id = ?;");
			statement.setInt(1, id);
			statement.executeUpdate();
			
			statement = this.getBdd().prepareStatement("delete from inscrire where classe_id = ?;");
			statement.setInt(1, id);
			statement.executeUpdate();
			
			statement = this.getBdd().prepareStatement("delete from classe where classe_id = ?;");
			statement.setInt(1, id);
			statement.executeUpdate();
			
		} catch (SQLException ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		return true;
	}
}
