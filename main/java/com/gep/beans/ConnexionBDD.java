package com.gep.beans;

import java.sql.*;

public class ConnexionBDD {
	private Connection bdd;
	
	public Connection getBdd() {
		return bdd;
	}

	public static Connection connexion() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
	        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/gestionecoleprimaire?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC","root","");
	        return con;
        } catch (Exception ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            return null;
        }
	}
}
	