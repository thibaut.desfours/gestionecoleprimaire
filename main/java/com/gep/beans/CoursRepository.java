package com.gep.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CoursRepository {
	private Connection bdd;
	Format formatter = new SimpleDateFormat("yyyy-mm-dd");
	public CoursRepository() {
		super();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.bdd = ConnexionBDD.connexion();
	}
	
	public int countAll() {
		int result = 0;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select count(*) as nb from cours");
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	result = rs.getInt("nb");
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return result;
	}

	public int countAllAbsences() {
		int result = 0;
		try {
			String query="select count(*) as nb from cours "
					+ "join inscrire using (cours_id) "
					+ "join classe using (classe_id) "
					+ "join appartenir using (classe_id) "
					+ "join eleve using (eleve_id) "
					+ "left join etre_present on etre_present.eleve_id = eleve.eleve_id "
					+ "where etre_present.eleve_id is null";
			PreparedStatement statement = this.getBdd().prepareStatement(query);
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	result = rs.getInt("nb");
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return result;
	}
	
	public ArrayList<Cours> getAll(){
		ClasseRepository classeR = new ClasseRepository();
		ArrayList<Cours> cours = new ArrayList();
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from cours join matiere using (matiere_id) join inscrire using(cours_id);");
			ResultSet rs = statement.executeQuery();
	        while(rs.next())
	        	cours.add(new Cours(rs.getInt("cours_id"),rs.getString("intitule"),rs.getDate("dateD"),rs.getDate("dateF"),new Matiere(rs.getInt("matiere_id"),rs.getString("libelle")),classeR.getClasseById(rs.getInt("classe_id"))));
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return cours;
	}
	
	
	public Cours getCoursById(int id){
		ClasseRepository classeR = new ClasseRepository();
		Cours cours = null;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select * from cours join matiere using (matiere_id) join inscrire using(cours_id) where cours_id = ?;");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	cours = new Cours(rs.getInt("classe_id"),rs.getString("intitule"),rs.getDate("dateD"),rs.getDate("dateF"),new Matiere(rs.getInt("matiere_id"),rs.getString("libelle")),classeR.getClasseById(rs.getInt("classe_id")));
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return cours;
	}
	
	public boolean insertCours(Cours cours) {
		try {
			CoursRepository coursR = new CoursRepository();
			PreparedStatement statement = this.getBdd().prepareStatement("insert into cours values(null,?,?,?,?);");
			statement.setString(1,cours.getIntitule());
			statement.setString(2,formatter.format(cours.getDateD()));
			statement.setString(3,formatter.format(cours.getDateF()));
			statement.setInt(4,cours.getMatiere().getId());
			
			if(statement.executeUpdate() < 1)
				return false;
			
			statement = this.getBdd().prepareStatement("insert into inscrire values(?,?);");
			statement.setInt(1,cours.getClasse().getId());
			statement.setInt(2,coursR.getMaxId());
			
			
			if(statement.executeUpdate() < 1)
				return false;
			
		} catch (SQLException ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		return true;
	}
	
	public int getMaxId() {
		int retour = 0;
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("select max(cours_id) as maxId from cours;");
			ResultSet rs = statement.executeQuery();
	        if(rs.next())
	        	retour = rs.getInt("maxId");	        	
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
		}
		return retour;
	}
	
	public boolean updateCours(Cours cours) {
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("update cours set intitule = ?, dateD = ?, dateF = ?, matiere_id = ? where cours_id = ?;");
			statement.setString(1,cours.getIntitule());
			statement.setString(2,formatter.format(cours.getDateD()));
			statement.setString(3,formatter.format(cours.getDateF()));
			statement.setInt(4,cours.getMatiere().getId());
			statement.setInt(5,cours.getId());
			statement.executeUpdate();
			
			statement = this.getBdd().prepareStatement("update inscrire set classe_id = ? where cours_id = ?;");
			statement.setInt(1,cours.getClasse().getId());
			statement.setInt(2,cours.getId());
			statement.executeUpdate();
		} catch (SQLException ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		return true;
	}
	
	
	public boolean deleteCours(int id) {
		try {
			PreparedStatement statement = this.getBdd().prepareStatement("delete from inscrire where cours_id = ?;");
			statement.setInt(1,id);
			
			statement.executeUpdate();
			
			statement = this.getBdd().prepareStatement("delete from cours where cours_id = ?;");
			statement.setInt(1,id);
			
			statement.executeUpdate();
		} catch (SQLException ex) {
        	System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		return true;
	}
	public Connection getBdd() {
		return bdd;
	}

	public void setBdd(Connection bdd) {
		this.bdd = bdd;
	}
}
	
	
