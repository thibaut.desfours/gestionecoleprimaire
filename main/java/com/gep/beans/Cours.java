package com.gep.beans;

import java.util.Date;

public class Cours {
	private int id;
	private String intitule;
	private Date dateD;
	private Date dateF;
	private Matiere matiere;
	private Classe classe;
	
	public Cours(int id, String intitule, Date dateD, Date dateF, Matiere matiere, Classe classe) {
		super();
		this.id = id;
		this.intitule = intitule;
		this.dateD = dateD;
		this.dateF = dateF;
		this.matiere = matiere;
		this.classe = classe;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public Date getDateD() {
		return dateD;
	}

	public void setDateD(Date dateD) {
		this.dateD = dateD;
	}

	public Date getDateF() {
		return dateF;
	}

	public void setDateF(Date dateF) {
		this.dateF = dateF;
	}

	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}
	
	
}
