package com.gep.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.CoursRepository;
import com.gep.beans.EleveRepository;
import com.gep.beans.Evaluation;
import com.gep.beans.EvaluationRepository;

public class Notes extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EleveRepository eleveR = new EleveRepository();
		EvaluationRepository evaluationR = new EvaluationRepository();
		Evaluation eval = evaluationR.getEvaluationById(Integer.parseInt(request.getParameter("id")));
		/* Transmission de la paire d'objets request/responseà notre JSP */
		request.setAttribute("evaluation", eval);
		request.setAttribute("eleves", eval.getCours().getClasse().getEleves());
		request.setAttribute("notes", evaluationR.getNotesByEvaluation(Integer.parseInt(request.getParameter("id"))));
		this.getServletContext().getRequestDispatcher("/Notes.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EvaluationRepository evaluationR = new EvaluationRepository();
		evaluationR.updateNote(Integer.parseInt(request.getParameter("eleveId")), Integer.parseInt(request.getParameter("evalId")), Float.parseFloat(request.getParameter("note")));
		/* Transmission de la paire d'objets request/responseà notre JSP */
		this.getServletContext().getRequestDispatcher("/Notes.jsp").forward(request, response);
	}
}
