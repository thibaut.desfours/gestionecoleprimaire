package com.gep.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.gep.beans.ClasseRepository;
import com.gep.beans.Eleve;
import com.gep.beans.EleveRepository;


public class AddStudent extends HttpServlet {
	DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClasseRepository classeR = new ClasseRepository();
		request.setAttribute("classes", classeR.getAll());
		/* Transmission de la paire d'objets request/responseà notre JSP */
	    this.getServletContext().getRequestDispatcher("/addStudent.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClasseRepository classeR = new ClasseRepository();
		EleveRepository eleveR = new EleveRepository();
		request.setCharacterEncoding("UTF-8");
		try {
			Eleve eleve = new Eleve(0,(String) request.getParameter("nom"),(String) request.getParameter("prenom"),(String) request.getParameter("adresse"),(String) request.getParameter("mailParents"),(String) request.getParameter("telephoneParents"),(String) request.getParameter("ville"), format.parse(request.getParameter("dateNaissance")),classeR.getClasseById(Integer.parseInt(request.getParameter("selectClasse"))));
			eleveR.insertEleve(eleve);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/* Transmission de la paire d'objets request/responseà notre JSP */
		
		request.setAttribute("eleves", eleveR.getAll());
		this.getServletContext().getRequestDispatcher("/students.jsp").forward(request, response);
	}
}