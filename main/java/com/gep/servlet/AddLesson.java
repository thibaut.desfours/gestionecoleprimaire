package com.gep.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.ClasseRepository;
import com.gep.beans.Cours;
import com.gep.beans.CoursRepository;
import com.gep.beans.MatiereRepository;

public class AddLesson extends HttpServlet {
	DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MatiereRepository matiereR = new MatiereRepository();
		ClasseRepository classeR = new ClasseRepository();
		
		request.setAttribute("classes", classeR.getAll());
		request.setAttribute("matieres", matiereR.getAll());
		/* Transmission de la paire d'objets request/responseà notre JSP */
		this.getServletContext().getRequestDispatcher("/addLesson.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoursRepository coursR = new CoursRepository();
		MatiereRepository matiereR = new MatiereRepository();
		ClasseRepository classeR = new ClasseRepository();
		request.setCharacterEncoding("UTF-8");
		try {
			coursR.insertCours(new Cours(0, request.getParameter("intitule"),format.parse(request.getParameter("dateD")),format.parse(request.getParameter("dateF")),matiereR.getMatiereById(Integer.parseInt(request.getParameter("selectMatiere"))),classeR.getClasseById(Integer.parseInt(request.getParameter("selectClasse")))));
		} catch (NumberFormatException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    /* Transmission de la paire d'objets request/responseà notre JSP */
		request.setAttribute("coursL", coursR.getAll());
		this.getServletContext().getRequestDispatcher("/lessons.jsp").forward(request, response);
	}
}