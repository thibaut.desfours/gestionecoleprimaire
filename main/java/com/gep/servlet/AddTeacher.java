package com.gep.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.ClasseRepository;
import com.gep.beans.Prof;
import com.gep.beans.ProfRepository;

public class AddTeacher extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		ClasseRepository classeR = new ClasseRepository();
		request.setAttribute("classes", classeR.getAll());
	    /* Transmission de la paire d'objets request/responseà notre JSP */
	    this.getServletContext().getRequestDispatcher("/addTeacher.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		ClasseRepository classeR = new ClasseRepository();
		ProfRepository profR = new ProfRepository();
		request.setCharacterEncoding("UTF-8");
		Prof prof = new Prof(0,(String) request.getParameter("nom"),(String) request.getParameter("prenom"),(String) request.getParameter("adresse"),(String) request.getParameter("mail"),(String) request.getParameter("telephone"),(String) request.getParameter("ville"),null);
		profR.insertProf(prof);
	    /* Transmission de la paire d'objets request/responseà notre JSP */
		request.setAttribute("profs", profR.getAll());
	    this.getServletContext().getRequestDispatcher("/teachers.jsp").forward(request, response);
	}
}