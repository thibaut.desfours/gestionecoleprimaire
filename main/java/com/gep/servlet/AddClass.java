package com.gep.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.Classe;
import com.gep.beans.ClasseRepository;
import com.gep.beans.EleveRepository;
import com.gep.beans.NiveauRepository;
import com.gep.beans.ProfRepository;

public class AddClass extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NiveauRepository niveauR = new NiveauRepository();
		ProfRepository profR = new ProfRepository();
		EleveRepository eleveR = new EleveRepository();
		request.setAttribute("niveaux", niveauR.getAll());
		request.setAttribute("profs", profR.getAllWithoutClass());
		/* Transmission de la paire d'objets request/responseà notre JSP */
		this.getServletContext().getRequestDispatcher("/addClass.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClasseRepository classeR = new ClasseRepository();
		NiveauRepository niveauR = new NiveauRepository();
		ProfRepository profR = new ProfRepository();
		request.setCharacterEncoding("UTF-8");
		classeR.insertClasse(new Classe(0,request.getParameter("nom"),Integer.parseInt(request.getParameter("annee")),profR.getById(Integer.parseInt(request.getParameter("selectProf"))),niveauR.getById(Integer.parseInt(request.getParameter("selectNiveau"))),null));
		/* Transmission de la paire d'objets request/responseà notre JSP */
		request.setAttribute("classes",classeR.getAll());
		this.getServletContext().getRequestDispatcher("/classes.jsp").forward(request, response);
	}
}