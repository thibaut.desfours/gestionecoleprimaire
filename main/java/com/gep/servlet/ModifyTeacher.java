package com.gep.servlet;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.ClasseRepository;
import com.gep.beans.Eleve;
import com.gep.beans.Prof;
import com.gep.beans.ProfRepository;

public class ModifyTeacher extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProfRepository profR = new ProfRepository();
		ClasseRepository classeR = new ClasseRepository();		
		request.setAttribute("prof", profR.getById(Integer.parseInt(request.getParameter("id"))));
		request.setAttribute("classes", classeR.getAll());
	    /* Transmission de la paire d'objets request/responseà notre JSP */
	    this.getServletContext().getRequestDispatcher("/modifyTeacher.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProfRepository profR = new ProfRepository();
		request.setCharacterEncoding("UTF-8");
		Prof prof = new Prof(Integer.parseInt(request.getParameter("id")),(String) request.getParameter("nom"),(String) request.getParameter("prenom"),(String) request.getParameter("adresse"),(String) request.getParameter("mail"),(String) request.getParameter("telephone"),(String) request.getParameter("ville"),null);
	    profR.updateProf(prof);
		/* Transmission de la paire d'objets request/responseà notre JSP */
	    doGet(request, response);
	}
}