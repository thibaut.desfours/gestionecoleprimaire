package com.gep.servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.ClasseRepository;
import com.gep.beans.ConnexionBDD;
import com.gep.beans.CoursRepository;
import com.gep.beans.EleveRepository;
import com.gep.beans.EvaluationRepository;
import com.gep.beans.ProfRepository;

public class Home extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	EleveRepository eleveR = new EleveRepository();
	ClasseRepository classeR = new ClasseRepository();
	ProfRepository profR = new ProfRepository();
	EvaluationRepository evalR = new EvaluationRepository();
	CoursRepository coursR = new CoursRepository();
	
	

	request.setAttribute("nbEleve",eleveR.countAll());
	request.setAttribute("nbClasse",classeR.countAll());
	request.setAttribute("nbProf",profR.countAll());
	request.setAttribute("nbAbsence",eleveR.countAll());
	request.setAttribute("nbCours",coursR.countAll());
	request.setAttribute("nbEval",evalR.countAll());
	request.setAttribute("nbAbs",coursR.countAllAbsences());
	
    /* Transmission de la paire d'objets request/responseà notre JSP */
    this.getServletContext().getRequestDispatcher("/home.jsp").forward(request, response);
  }
}