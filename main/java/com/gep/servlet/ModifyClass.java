package com.gep.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.Classe;
import com.gep.beans.ClasseRepository;
import com.gep.beans.Eleve;
import com.gep.beans.EleveRepository;
import com.gep.beans.NiveauRepository;
import com.gep.beans.ProfRepository;

public class ModifyClass extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClasseRepository classeR = new ClasseRepository();
		NiveauRepository niveauR = new NiveauRepository();
		ProfRepository profR = new ProfRepository();
		EleveRepository eleveR = new EleveRepository();
		request.setAttribute("classe", classeR.getClasseById(Integer.parseInt(request.getParameter("id"))));
		request.setAttribute("niveaux", niveauR.getAll());
		request.setAttribute("profs", profR.getAllWithoutClass());
		request.setAttribute("eleves", eleveR.getAllWithoutClass());
		/* Transmission de la paire d'objets request/responseà notre JSP */
	    this.getServletContext().getRequestDispatcher("/modifyClass.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClasseRepository classeR = new ClasseRepository();
		ProfRepository profR = new ProfRepository();
		EleveRepository eleveR = new EleveRepository();
		NiveauRepository niveauR = new NiveauRepository();
		request.setCharacterEncoding("UTF-8");
		
		if(request.getParameter("delId") != null && request.getParameter("classeId")!= null)
			classeR.retirerEleve(Integer.parseInt(request.getParameter("delId")), Integer.parseInt(request.getParameter("classeId")));
		else if(request.getParameter("Cid") != null && request.getParameter("nom") != null && request.getParameter("annee")!= null && request.getParameter("selectNiveau")!= null && request.getParameter("selectProf")!= null)
			classeR.updateClasse(new Classe(Integer.parseInt(request.getParameter("Cid")),(String) request.getParameter("nom"), Integer.parseInt(request.getParameter("annee")), profR.getById(Integer.parseInt(request.getParameter("selectProf"))),niveauR.getById(Integer.parseInt(request.getParameter("selectNiveau"))),null));
		else if(request.getParameter("listEleve") != null && request.getParameter("classeIdAddEleve") != null) {
			ArrayList<Integer> eleves = new ArrayList();
			String [] listEleve = request.getParameter("listEleve").split(",");
			for(String eleve : listEleve)
				eleves.add(Integer.parseInt(eleve));
			eleveR.addClass(eleves,Integer.parseInt(request.getParameter("classeIdAddEleve")));
		}
		/* Transmission de la paire d'objets request/responseà notre JSP */
	    doGet(request,response);
	}
}