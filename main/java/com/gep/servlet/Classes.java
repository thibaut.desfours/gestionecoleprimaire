package com.gep.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.ClasseRepository;

public class Classes extends HttpServlet {
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClasseRepository classeR = new ClasseRepository();
		
		request.setAttribute("classes",classeR.getAll());
		/* Transmission de la paire d'objets request/responseà notre JSP */
	    this.getServletContext().getRequestDispatcher("/classes.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClasseRepository classeR = new ClasseRepository();
		request.setCharacterEncoding("UTF-8");
		classeR.deleteClasse(Integer.parseInt(request.getParameter("delId")));		
		/* Transmission de la paire d'objets request/responseà notre JSP */
	    doGet(request, response);
	}
}