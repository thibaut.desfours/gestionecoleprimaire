package com.gep.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.CoursRepository;

public class Lessons extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    CoursRepository coursR = new CoursRepository();
	    request.setAttribute("coursL", coursR.getAll());
		/* Transmission de la paire d'objets request/responseà notre JSP */
	    this.getServletContext().getRequestDispatcher("/lessons.jsp").forward(request, response);
    }
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    CoursRepository coursR = new CoursRepository();
	    coursR.deleteCours(Integer.parseInt(request.getParameter("delId")));
		/* Transmission de la paire d'objets request/responseà notre JSP */
	    doGet(request, response);
    }
}