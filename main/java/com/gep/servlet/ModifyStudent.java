package com.gep.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.ClasseRepository;
import com.gep.beans.Eleve;
import com.gep.beans.EleveRepository;

public class ModifyStudent extends HttpServlet {
	DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EleveRepository eleveR = new EleveRepository();	
		ClasseRepository classeR = new ClasseRepository();
		request.setAttribute("eleve", eleveR.getById(Integer.parseInt(request.getParameter("id"))));
		request.setAttribute("classes", classeR.getAll());
		/* Transmission de la paire d'objets request/responseà notre JSP */
		this.getServletContext().getRequestDispatcher("/modifyStudent.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EleveRepository eleveR = new EleveRepository();
		ClasseRepository classeR = new ClasseRepository();
		request.setCharacterEncoding("UTF-8");
		try {
			Eleve eleve = new Eleve(Integer.parseInt(request.getParameter("id")),(String) request.getParameter("nom"),(String) request.getParameter("prenom"),(String) request.getParameter("adresse"),(String) request.getParameter("mailParents"),(String) request.getParameter("telephoneParents"),(String) request.getParameter("ville"), format.parse(request.getParameter("dateNaissance")),classeR.getClasseById(Integer.parseInt(request.getParameter("selectClasse"))));
			eleveR.updateEleve(eleve);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/* Transmission de la paire d'objets request/responseà notre JSP */
		doGet(request,response);
  }
}