package com.gep.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.EleveRepository;
import com.gep.beans.Prof;
import com.gep.beans.ProfRepository;

public class Teachers extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProfRepository profR = new ProfRepository();
		request.setAttribute("profs", profR.getAll());
	    /* Transmission de la paire d'objets request/responseà notre JSP */
	    this.getServletContext().getRequestDispatcher("/teachers.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProfRepository profR = new ProfRepository();
		profR.deleteProf(Integer.parseInt(request.getParameter("delId")));
		request.setAttribute("profs", profR.getAll());
	    /* Transmission de la paire d'objets request/responseà notre JSP */
	    this.getServletContext().getRequestDispatcher("/teachers.jsp").forward(request, response);
	}
}