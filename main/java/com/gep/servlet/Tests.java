package com.gep.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.EvaluationRepository;

public class Tests extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  	EvaluationRepository evaluationR = new EvaluationRepository();
	  	
	  	request.setAttribute("evaluations", evaluationR.getAll());
		/* Transmission de la paire d'objets request/responseà notre JSP */
	   	this.getServletContext().getRequestDispatcher("/tests.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  	EvaluationRepository evaluationR = new EvaluationRepository();
	  	evaluationR.deleteEvaluation(Integer.parseInt(request.getParameter("delId")));
	  	
		/* Transmission de la paire d'objets request/responseà notre JSP */
	   	doGet(request, response);
	}
}