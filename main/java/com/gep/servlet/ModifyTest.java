package com.gep.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.CoursRepository;
import com.gep.beans.Evaluation;
import com.gep.beans.EvaluationRepository;

public class ModifyTest extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EvaluationRepository evaluationR = new EvaluationRepository();
		CoursRepository coursR = new CoursRepository();
		request.setAttribute("evaluation", evaluationR.getEvaluationById(Integer.parseInt(request.getParameter("id"))));
		request.setAttribute("cours", coursR.getAll());
		/* Transmission de la paire d'objets request/responseà notre JSP */
		this.getServletContext().getRequestDispatcher("/modifyTest.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EvaluationRepository evaluationR = new EvaluationRepository();
		CoursRepository coursR = new CoursRepository();
		
		evaluationR.updateEvaluation(new Evaluation(Integer.parseInt(request.getParameter("id")),request.getParameter("intitule"),coursR.getCoursById(Integer.parseInt(request.getParameter("selectCours")))));
		/* Transmission de la paire d'objets request/responseà notre JSP */
		doGet(request, response);
	}
}