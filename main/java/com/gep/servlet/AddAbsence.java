package com.gep.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddAbsence extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /* Transmission de la paire d'objets request/responseà notre JSP */
    this.getServletContext().getRequestDispatcher("/addAbsence.jsp").forward(request, response);
  }
}