package com.gep.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.CoursRepository;
import com.gep.beans.Evaluation;
import com.gep.beans.EvaluationRepository;

public class AddTest extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoursRepository coursR = new CoursRepository();
		request.setAttribute("cours", coursR.getAll());
	    /* Transmission de la paire d'objets request/responseà notre JSP */
	    this.getServletContext().getRequestDispatcher("/addTest.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoursRepository coursR = new CoursRepository();
		EvaluationRepository evaluationR = new EvaluationRepository();
		evaluationR.insertEvaluation(new Evaluation(0,request.getParameter("intitule"),coursR.getCoursById(Integer.parseInt(request.getParameter("selectCours")))));
	    /* Transmission de la paire d'objets request/responseà notre JSP */
	  	
	  	request.setAttribute("evaluations", evaluationR.getAll());
	    this.getServletContext().getRequestDispatcher("/tests.jsp").forward(request, response);
	}
}