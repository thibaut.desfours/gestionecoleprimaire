package com.gep.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gep.beans.ClasseRepository;
import com.gep.beans.Cours;
import com.gep.beans.CoursRepository;
import com.gep.beans.MatiereRepository;

public class ModifyLesson extends HttpServlet {
	DateFormat format = new SimpleDateFormat("yyyy-mm-dd");

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoursRepository coursR = new CoursRepository();
		MatiereRepository matiereR = new MatiereRepository();
		ClasseRepository classeR = new ClasseRepository();
		
		request.setAttribute("cours", coursR.getCoursById(Integer.parseInt(request.getParameter("id"))));
		request.setAttribute("classes", classeR.getAll());
		request.setAttribute("matieres", matiereR.getAll());
	    /* Transmission de la paire d'objets request/responseà notre JSP */
	    this.getServletContext().getRequestDispatcher("/modifyLesson.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoursRepository coursR = new CoursRepository();
		MatiereRepository matiereR = new MatiereRepository();
		ClasseRepository classeR = new ClasseRepository();
		request.setCharacterEncoding("UTF-8");
		try {
			coursR.updateCours(new Cours(Integer.parseInt(request.getParameter("cours_id")), request.getParameter("intitule"),format.parse(request.getParameter("dateD")),format.parse(request.getParameter("dateF")),matiereR.getMatiereById(Integer.parseInt(request.getParameter("selectMatiere"))),classeR.getClasseById(Integer.parseInt(request.getParameter("selectClasse")))));
		} catch (NumberFormatException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    /* Transmission de la paire d'objets request/responseà notre JSP */
	    doGet(request, response);
	}
}