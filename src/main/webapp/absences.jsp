<%@page contentType="text/html"
        pageEncoding="UTF-8"
%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/ico" href="img/favicon.gif" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/list.css">
<link rel="stylesheet" href="css/sidebar.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<title>Les élèves</title>
</head>

<body>
    <%@include file="navbar.jsp" %>
    <%@include file="sidebar.jsp" %>

    <div id="main">
        <div id="titleAndButton">
            <div id="title">
                <h2>Les absences :</h2>
            </div>
            <div id="button"><button id="addButton" type="button" class="btn"><img src="images/Plus.svg"
                        alt="">Ajouter une absence</button></div>
        </div>
        <table id="table" class="display">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Elève</th>
                    <th>Classe</th>
                    <th>Date</th>
                    <th>Cours</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Powell Jacob</td>
                    <td>CP1</td>
                    <td>26/08/2020</td>
                    <td>Algèbre</td>
                    <td>
                        <button class="viewButton btn" type="button"><img src="images/view.svg" alt=""></button>
                        <button class="modifyButton btn" type="button"><img src="images/modify.svg" alt=""></button>
                        <button class="deleteButton btn" type="button"><img src="images/delete.svg" alt=""></button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div id="deleteModal" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Suppression d'une absence</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Êtes-vous sûr de vouloir supprimer cette absence ?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Annuler</button>
					<button type="button" class="btn btn-danger">Supprimer</button>
				</div>
			</div>
		</div>
	</div>

	<script src="js/sidebar.js" type="text/javascript"></script>
    <script>
		$(document)
				.ready(
						function() {
							$('#table')
									.DataTable(
											{
												"language" : {
													"url" : "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
												}
											});
						});
		
		// Delete modal
		var elements = document.getElementsByClassName("deleteButton");
		
		var deleteModal = function() {
			$('#deleteModal').modal('show');
		}
		
		for (var i = 0; i < elements.length; i++) {
		    elements[i].addEventListener('click', deleteModal, false);
		}
	</script>
</body>

</html>