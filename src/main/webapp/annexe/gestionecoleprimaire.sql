-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 20 sep. 2020 à 15:38
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gestionecoleprimaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `appartenir`
--

DROP TABLE IF EXISTS `appartenir`;
CREATE TABLE IF NOT EXISTS `appartenir` (
  `eleve_id` int(11) NOT NULL,
  `classe_id` int(11) NOT NULL,
  `Annee` year(4) NOT NULL,
  PRIMARY KEY (`eleve_id`,`classe_id`),
  KEY `appartenir_FK_1` (`classe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `appartenir`
--

INSERT INTO `appartenir` (`eleve_id`, `classe_id`, `Annee`) VALUES
(1, 1, 2020),
(2, 1, 2020);

-- --------------------------------------------------------

--
-- Structure de la table `categoriemateriel`
--

DROP TABLE IF EXISTS `categoriemateriel`;
CREATE TABLE IF NOT EXISTS `categoriemateriel` (
  `id_categorieM` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(80) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_categorieM`),
  KEY `categoriemateriel_FK` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

DROP TABLE IF EXISTS `classe`;
CREATE TABLE IF NOT EXISTS `classe` (
  `classe_id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `Annee` year(4) NOT NULL,
  `prof_id` int(11) DEFAULT NULL,
  `niveau_id` int(11) NOT NULL,
  PRIMARY KEY (`classe_id`),
  KEY `Classe_Prof0_FK` (`prof_id`),
  KEY `Classe_Niveau1_FK` (`niveau_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`classe_id`, `nom`, `Annee`, `prof_id`, `niveau_id`) VALUES
(1, 'RIL2020', 2020, 6, 1);

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

DROP TABLE IF EXISTS `cours`;
CREATE TABLE IF NOT EXISTS `cours` (
  `cours_id` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(80) NOT NULL,
  `dateD` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dateF` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `matiere_id` int(11) NOT NULL,
  PRIMARY KEY (`cours_id`),
  KEY `Cours_Matiere0_FK` (`matiere_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `cours`
--

INSERT INTO `cours` (`cours_id`, `intitule`, `dateD`, `dateF`, `matiere_id`) VALUES
(1, 'JEE c\'est cool', '2020-08-28 22:00:00', '2020-08-30 22:00:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `eleve`
--

DROP TABLE IF EXISTS `eleve`;
CREATE TABLE IF NOT EXISTS `eleve` (
  `eleve_id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `dateN` date NOT NULL,
  `adresseResp` varchar(100) DEFAULT NULL,
  `telResp` varchar(10) DEFAULT NULL,
  `villeResp` varchar(40) DEFAULT NULL,
  `mailResp` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`eleve_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `eleve`
--

INSERT INTO `eleve` (`eleve_id`, `nom`, `prenom`, `dateN`, `adresseResp`, `telResp`, `villeResp`, `mailResp`) VALUES
(1, 'Brel', 'Jacques', '1929-01-08', 'Ã?les Marquises', '0652642392', 'Colombiers', 'jacques.brel@gmail.com'),
(2, 'Piaf', 'Édith', '1915-12-19', 'Test adr', '0652642392', 'Colombiers', 'jacques.brel@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `etre_present`
--

DROP TABLE IF EXISTS `etre_present`;
CREATE TABLE IF NOT EXISTS `etre_present` (
  `cours_id` int(11) NOT NULL,
  `eleve_id` int(11) NOT NULL,
  PRIMARY KEY (`cours_id`,`eleve_id`),
  KEY `etre_present_FK_1` (`eleve_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etre_present`
--

INSERT INTO `etre_present` (`cours_id`, `eleve_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `evaluation`
--

DROP TABLE IF EXISTS `evaluation`;
CREATE TABLE IF NOT EXISTS `evaluation` (
  `evaluation_id` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(80) NOT NULL,
  `cours_id` int(11) NOT NULL,
  PRIMARY KEY (`evaluation_id`),
  KEY `evaluation_FK` (`cours_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `evaluation`
--

INSERT INTO `evaluation` (`evaluation_id`, `intitule`, `cours_id`) VALUES
(8, 'Test JEE', 1);

-- --------------------------------------------------------

--
-- Structure de la table `inscrire`
--

DROP TABLE IF EXISTS `inscrire`;
CREATE TABLE IF NOT EXISTS `inscrire` (
  `classe_id` int(11) NOT NULL,
  `cours_id` int(11) NOT NULL,
  PRIMARY KEY (`classe_id`,`cours_id`),
  KEY `inscrire_FK_1` (`cours_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `inscrire`
--

INSERT INTO `inscrire` (`classe_id`, `cours_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `materiel`
--

DROP TABLE IF EXISTS `materiel`;
CREATE TABLE IF NOT EXISTS `materiel` (
  `materiel_id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(80) NOT NULL,
  `id_categorieM` int(11) NOT NULL,
  PRIMARY KEY (`materiel_id`),
  KEY `materiel_FK` (`id_categorieM`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

DROP TABLE IF EXISTS `matiere`;
CREATE TABLE IF NOT EXISTS `matiere` (
  `matiere_id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(80) NOT NULL,
  PRIMARY KEY (`matiere_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`matiere_id`, `libelle`) VALUES
(1, 'Prog');

-- --------------------------------------------------------

--
-- Structure de la table `niveau`
--

DROP TABLE IF EXISTS `niveau`;
CREATE TABLE IF NOT EXISTS `niveau` (
  `niveau_id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) NOT NULL,
  PRIMARY KEY (`niveau_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `niveau`
--

INSERT INTO `niveau` (`niveau_id`, `libelle`) VALUES
(1, 'CP');

-- --------------------------------------------------------

--
-- Structure de la table `passer`
--

DROP TABLE IF EXISTS `passer`;
CREATE TABLE IF NOT EXISTS `passer` (
  `eleve_id` int(11) NOT NULL,
  `evaluation_id` int(11) NOT NULL,
  `note` float DEFAULT NULL,
  PRIMARY KEY (`eleve_id`,`evaluation_id`),
  KEY `passer_FK_1` (`evaluation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `passer`
--

INSERT INTO `passer` (`eleve_id`, `evaluation_id`, `note`) VALUES
(1, 8, 0),
(2, 8, 15);

-- --------------------------------------------------------

--
-- Structure de la table `prof`
--

DROP TABLE IF EXISTS `prof`;
CREATE TABLE IF NOT EXISTS `prof` (
  `prof_id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `adresse` varchar(100) NOT NULL,
  `ville` varchar(100) NOT NULL,
  PRIMARY KEY (`prof_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `prof`
--

INSERT INTO `prof` (`prof_id`, `nom`, `prenom`, `tel`, `mail`, `adresse`, `ville`) VALUES
(5, 'testNom', 'testPrénom', '0652642598', 'test.test@viacesi.fr', 'fsdgfdhgfdh', 'gfhfghfghfgh'),
(6, 'sftest', 'fgdgdf', '059874575', 'fdhgdgfhg', 'hgfhfg', 'dsfsdfsdf');

-- --------------------------------------------------------

--
-- Structure de la table `reserver`
--

DROP TABLE IF EXISTS `reserver`;
CREATE TABLE IF NOT EXISTS `reserver` (
  `cours_id` int(11) NOT NULL,
  `materiel_id` int(11) NOT NULL,
  `Quantite` int(11) NOT NULL,
  PRIMARY KEY (`cours_id`,`materiel_id`),
  KEY `reserver_FK` (`materiel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `appartenir`
--
ALTER TABLE `appartenir`
  ADD CONSTRAINT `appartenir_FK` FOREIGN KEY (`eleve_id`) REFERENCES `eleve` (`eleve_id`),
  ADD CONSTRAINT `appartenir_FK_1` FOREIGN KEY (`classe_id`) REFERENCES `classe` (`classe_id`);

--
-- Contraintes pour la table `categoriemateriel`
--
ALTER TABLE `categoriemateriel`
  ADD CONSTRAINT `categoriemateriel_FK` FOREIGN KEY (`parent_id`) REFERENCES `categoriemateriel` (`id_categorieM`);

--
-- Contraintes pour la table `classe`
--
ALTER TABLE `classe`
  ADD CONSTRAINT `Classe_Niveau1_FK` FOREIGN KEY (`niveau_id`) REFERENCES `niveau` (`niveau_id`),
  ADD CONSTRAINT `Classe_Prof0_FK` FOREIGN KEY (`prof_id`) REFERENCES `prof` (`prof_id`);

--
-- Contraintes pour la table `cours`
--
ALTER TABLE `cours`
  ADD CONSTRAINT `Cours_Matiere0_FK` FOREIGN KEY (`matiere_id`) REFERENCES `matiere` (`matiere_id`);

--
-- Contraintes pour la table `etre_present`
--
ALTER TABLE `etre_present`
  ADD CONSTRAINT `etre_present_FK` FOREIGN KEY (`cours_id`) REFERENCES `cours` (`cours_id`),
  ADD CONSTRAINT `etre_present_FK_1` FOREIGN KEY (`eleve_id`) REFERENCES `eleve` (`eleve_id`);

--
-- Contraintes pour la table `evaluation`
--
ALTER TABLE `evaluation`
  ADD CONSTRAINT `evaluation_FK` FOREIGN KEY (`cours_id`) REFERENCES `cours` (`cours_id`);

--
-- Contraintes pour la table `inscrire`
--
ALTER TABLE `inscrire`
  ADD CONSTRAINT `inscrire_FK` FOREIGN KEY (`classe_id`) REFERENCES `classe` (`classe_id`),
  ADD CONSTRAINT `inscrire_FK_1` FOREIGN KEY (`cours_id`) REFERENCES `cours` (`cours_id`);

--
-- Contraintes pour la table `materiel`
--
ALTER TABLE `materiel`
  ADD CONSTRAINT `materiel_FK` FOREIGN KEY (`id_categorieM`) REFERENCES `categoriemateriel` (`id_categorieM`);

--
-- Contraintes pour la table `passer`
--
ALTER TABLE `passer`
  ADD CONSTRAINT `passer_FK` FOREIGN KEY (`eleve_id`) REFERENCES `eleve` (`eleve_id`),
  ADD CONSTRAINT `passer_FK_1` FOREIGN KEY (`evaluation_id`) REFERENCES `evaluation` (`evaluation_id`);

--
-- Contraintes pour la table `reserver`
--
ALTER TABLE `reserver`
  ADD CONSTRAINT `reserver_FK` FOREIGN KEY (`materiel_id`) REFERENCES `materiel` (`materiel_id`),
  ADD CONSTRAINT `reserver_FK_1` FOREIGN KEY (`cours_id`) REFERENCES `cours` (`cours_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
