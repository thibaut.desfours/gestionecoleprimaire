<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.gep.beans.Classe" %>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/ico" href="img/favicon.gif" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/add.css">
<link rel="stylesheet" href="css/sidebar.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<title>Ajout d'un élève</title>
</head>

<body>
	<%@include file="navbar.jsp" %>
    <%@include file="sidebar.jsp" %>
	<form method="post" >
	<div id="main">
		<h2>Ajouter un élève</h2>
		<label>Nom</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="nom" name="nom"
				aria-describedby="basic-addon3">
		</div>
		<label>Prénom</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="prenom" name="prenom"
				aria-describedby="basic-addon3">
		</div>
		<label>Date de naissance</label> <input id="dateNaissance" type="text"
			class="form-control" name="dateNaissance"> <label class="label">Classe</label><br>
		<select class="form-control" name="selectClasse">
			<%
				ArrayList<Classe> classes = (ArrayList<Classe>) request.getAttribute("classes");
				for(Classe classe : classes){
					out.println("<option value='"+classe.getId()+"'>"+classe.getNom()+"</option>");
				}
			%>
		</select>
		<br> <label>Téléphone des parents</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="telephoneParents" name="telephoneParents"
				aria-describedby="basic-addon3">
		</div>
		<label>Mail des parents</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="mailParents" name="mailParents"
				aria-describedby="basic-addon3">
		</div>
		<label>Adresse</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="adresse" name="adresse"
				aria-describedby="basic-addon3">
		</div>
		<label>Ville</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="ville" name="ville"
				aria-describedby="basic-addon3">
		</div>
		<div id="buttons">
			<button type="button" class="btn btn-secondary" onclick="window.location.href='./students'">
				Annuler et revenir à la liste
			</button>
			<button id="addButton" type="submit" class="btn">
				<img src="images/Plus.svg" alt="">Ajouter
			</button>
		</div>
	</div>
	</form>
	<script src="js/sidebar.js" type="text/javascript"></script>
	<script>
		// Datepicker
		$('#dateNaissance').datepicker({
			format : "yyyy-mm-dd",
			todayBtn : "linked",
			language : "fr",
			todayHighlight : true
		});
	</script>
</body>

</html>