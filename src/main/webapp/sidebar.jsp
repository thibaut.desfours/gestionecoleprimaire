<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="mySidebar" class="sidebar">
	<a href="./home"><img src="images/Home.svg" alt=""></a> 
	<a href="./tests"><img src="images/Tests.svg" alt=""></a> 
	<a href="./classes"><img src="images/Classes.svg" alt=""></a> 
	<a href="./teachers"><img src="images/Teachers.svg" alt=""></a> 
	<a href="./students"><img src="images/Students.svg" alt=""></a> 
	<a href="./absences"><img src="images/Absences.svg" alt=""></a> 
	<a href="./lessons"><img src="images/Lessons.svg" alt=""></a>
</div>