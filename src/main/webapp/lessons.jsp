<%@page contentType="text/html"
        pageEncoding="UTF-8"
%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.gep.beans.Cours" %>
<%@ page import="com.gep.beans.Matiere" %>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/ico" href="img/favicon.gif" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/list.css">
<link rel="stylesheet" href="css/sidebar.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<title>Les élèves</title>
</head>

<body>
    <%@include file="navbar.jsp" %>
    <%@include file="sidebar.jsp" %>

    <div id="main">
        <div id="titleAndButton">
            <div id="title">
                <h2>Les cours :</h2>
            </div>
            <div id="button"><button id="addButton" type="button" class="btn" onclick="window.location.href='./addLesson';"><img src="images/Plus.svg"
                        alt="">Ajouter un cours</button></div>
        </div>
        <table id="table" class="display">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Matière</th>
                    <th>Date</th>
                    <th>Classe</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            	<%
					ArrayList<Cours> coursL = (ArrayList<Cours>) request.getAttribute("coursL");
					for(Cours cours: coursL){
						out.println("<tr>");
						out.println("<td>"+cours.getId()+"</td>");
						out.println("<td>"+cours.getIntitule()+"</td>");
						out.println("<td>"+cours.getMatiere().getLibelle()+"</td>");
						out.println("<td>"+cours.getDateD()+"</td>");
						out.println("<td>"+cours.getClasse().getNom()+"</td>");
						out.println("<td><button class=\"modifyButton btn\" type=\"button\" onclick=\"window.location.href = './modifyLesson?id="+cours.getId()+"';\"><img src=\"images/modify.svg\" alt=\"\"></button>");
						out.println("<button class=\"deleteButton btn\" type=\"button\" row_id=\""+cours.getId()+"\"><img src=\"images/delete.svg\" alt=\"\"></button></td>");
						out.println("</tr>");
					}
				%>
            </tbody>
        </table>
    </div>

    <div id="deleteModal" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Suppression d'un cours</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Êtes-vous sûr de vouloir supprimer ce cours ?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Annuler</button>
					<form method="post">
						<input type="hidden" name="delId" id="delId" />
						<button type="submit" class="btn btn-danger">Supprimer</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script src="js/sidebar.js" type="text/javascript"></script>
    <script>
		$(document)
				.ready(
						function() {
							$('#table')
									.DataTable(
											{
												"language" : {
													"url" : "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
												}
											});
							$(".deleteButton").click(function(){
								$("#delId").val($(this).attr('row_id'));
							});
						});
		
		// Delete modal
		var elements = document.getElementsByClassName("deleteButton");
		
		var deleteModal = function() {
			$('#deleteModal').modal('show');
		}
		
		for (var i = 0; i < elements.length; i++) {
		    elements[i].addEventListener('click', deleteModal, false);
		}
	</script>
</body>

</html>