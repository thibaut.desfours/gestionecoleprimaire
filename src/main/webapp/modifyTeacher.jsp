<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.gep.beans.Prof" %>
<%@ page import="com.gep.beans.Classe" %>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/ico" href="img/favicon.gif" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/modify.css">
<link rel="stylesheet" href="css/sidebar.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<title>Modification d'un professeur</title>
</head>

<body>
	<%@include file="navbar.jsp" %>
    <%@include file="sidebar.jsp" %>

	<div id="main">
		<h2>Modification du professeur <% Prof prof = (Prof) request.getAttribute("prof"); out.println(prof.getPrenom()+" "+prof.getNom()); %></h2>
		<form method="post">
		<label>Nom</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="nom" name="nom" value="<% out.println(prof.getNom()); %>"
				aria-describedby="basic-addon3">
		</div>
		<label>Prénom</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="prenom" name="prenom" value="<% out.println(prof.getPrenom()); %>"
				aria-describedby="basic-addon3">
		</div>
		<br>
		<label>Téléphone</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="telephone" name="telephone" value="<% out.println(prof.getTel()); %>"
				aria-describedby="basic-addon3">
		</div>
		<label>Mail</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="mail" name="mail" value="<% out.println(prof.getMail()); %>"
				aria-describedby="basic-addon3">
		</div>
		<label>Adresse</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="adresse" name="adresse" value="<% out.println(prof.getAdresse()); %>"
				aria-describedby="basic-addon3">
		</div>
		<label>Ville</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="ville" name="ville" value="<% out.println(prof.getVille()); %>"
				aria-describedby="basic-addon3">
		</div>
		<div id="buttons">
			<button type="button" class="btn btn-secondary">Annuler et
				revenir à la liste</button>
			<button id="modifyButton" type="submit" class="btn">
				<img src="images/pencil.svg" alt="">Accepter les modifications
			</button>
		</div>
		</form>
	</div>

	<script src="js/sidebar.js" type="text/javascript"></script>
	<script>
		// Datepicker
		$('#dateNaissance').datepicker({
			format : "yyyy-mm-dd",
			todayBtn : "linked",
			language : "fr",
			todayHighlight : true
		});
	</script>
</body>

</html>