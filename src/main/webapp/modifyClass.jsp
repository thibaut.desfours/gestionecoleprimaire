<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.gep.beans.Prof" %>
<%@ page import="com.gep.beans.Classe" %>
<%@ page import="com.gep.beans.Niveau" %>
<%@ page import="com.gep.beans.Eleve" %>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/ico" href="img/favicon.gif" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/modify.css">
<link rel="stylesheet" href="css/sidebar.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<title>Modification d'une classe</title>
</head>

<body>
	<%@include file="navbar.jsp" %>
    <%@include file="sidebar.jsp" %>

	<div id="main">
		<h2>Modification de la classe <% Classe classe = (Classe) request.getAttribute("classe"); out.println(classe.getNom()); %></h2>
		<form method="post" name="formModif">
		<input type="hidden" name="Cid" <% out.println("value ='"+classe.getId()+"'"); %>/>
		<label>Nom</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="nom" name="nom" value="<% out.println(classe.getNom()); %>"
				aria-describedby="basic-addon3">
		</div>
		<label>Annee</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="annee" name="annee" <% out.println("value='"+classe.getAnnee()+"'");%>"
				aria-describedby="basic-addon3">
		</div>
		<label>Niveau</label><br>
		<select class="form-control" name="selectNiveau">
				<%
					ArrayList<Niveau> niveaux = (ArrayList<Niveau>) request.getAttribute("niveaux");
					for(Niveau niveau : niveaux){
						if(classe.getNiveau().getId() == niveau.getId())
							out.println("<option selected value='"+niveau.getId()+"'>"+niveau.getLibelle()+"</option>");
						else
							out.println("<option value='"+niveau.getId()+"'>"+niveau.getLibelle()+"</option>");
					}
				%>
			</select>
		<br> <label class="label">Professeur</label><br>
		<select class="form-control" name="selectProf">
				<%
					ArrayList<Prof> profs = (ArrayList<Prof>) request.getAttribute("profs");
					for(Prof prof : profs){
						if(classe.getProf().getId() == prof.getId())
							out.println("<option selected value='"+prof.getId()+"'>"+prof.getPrenom()+" "+prof.getNom()+"</option>");
						else
							out.println("<option value='"+prof.getId()+"'>"+prof.getPrenom()+" "+prof.getNom()+"</option>");
					}
				%>
			</select>
		<br>
		<div id="titleAndButton">
			<div id="title">
				<h2>Les élèves :</h2>
			</div>
			<div id="button">
				<button id="addButton" type="button" class="btn btnModalAdd">
					<img src="images/Plus.svg" alt="">Ajouter un élève
				</button>
			</div>
		</div>
		<table id="table" class="display">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nom</th>
					<th>Prénom</th>
					<th>Classe</th>
					<th>Date de naissance</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
					<%
					for(Eleve eleve : classe.getEleves()){
						out.println("<tr>");
						out.println("<td>"+eleve.getId()+"</td>");
						out.println("<td>"+eleve.getNom()+"</td>");
						out.println("<td>"+eleve.getPrenom()+"</td>");
						out.println("<td>"+eleve.getClasse().getNom()+"</td>");
						out.println("<td>"+eleve.getDateN()+"</td>");
						out.println("<td><button class=\"btnModalDel btn deleteButton\" type=\"button\" row_id=\""+eleve.getId()+"\"><img src=\"images/delete.svg\" alt=\"\"></button></td>");
						out.println("</tr>");
					}
					%>
			</tbody>
		</table>
		<br>
		<div id="buttons">
			<button type="button" class="btn btn-secondary">Annuler et
				revenir à la liste</button>
			<button id="modifyButton" type="submit" class="btn">
				<img src="images/pencil.svg" alt="">Accepter les modifications
			</button>
		</div>
		</form>
	</div>
	
	<div id="modalDel" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Suppression d'une classe</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p id="pDel">Êtes-vous sûr de vouloir supprimer cette classe ?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Annuler</button>
					<form method="post" name="formDel" id="formDel">
						<input type="hidden" name="delId" id="delId"/>
						<input type="hidden" name="classeId" id="classeId" <% out.println("value='"+classe.getId()+"'");%>/>
						<button type="submit" class="btn btn-danger">Supprimer</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modalAdd" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Ajouter des élèves à la classe</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="post" name="formAdd" id="formAdd">
					<p>Élèves :</p>
					<%
						ArrayList<Eleve> eleves = (ArrayList<Eleve>) request.getAttribute("eleves");
						for(Eleve eleve : eleves){
							out.println("<input type='checkbox' name='eleveCheck"+eleve.getId()+"' class = 'eleveCheck' value='"+eleve.getId()+"'/>");
							out.println("<label for='eleveCheck"+eleve.getId()+"'>"+eleve.getPrenom()+" "+eleve.getNom()+"</label></br>");
									
						}
						
					%>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Annuler</button>
						<input type="hidden" name="listEleve" id="listEleve"/>
						<input type="hidden" name="classeIdAddEleve" <%out.println("value='"+classe.getId()+"'"); %> />
						<button type="button" class="btn btn-danger" id="submitAdd">Ajouter</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script src="js/sidebar.js" type="text/javascript"></script>
	<script>
		$(document)
				.ready(
						function() {
							$('#table')
									.DataTable(
											{
												"language" : {
													"url" : "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
												}
											});
							$(".deleteButton").click(function(){
								$("#delId").val($(this).attr('row_id'));
							});
						});

		// Delete modal del
		var elementsDel = document.getElementsByClassName("btnModalDel");

		var modalDel = function() {
			$('#modalDel').modal('show');
		}

		for (var i = 0; i < elementsDel.length; i++) {
			elementsDel[i].addEventListener('click', modalDel, false);
		}
		
		// Delete modal add
		var elementsAdd = document.getElementsByClassName("btnModalAdd");

		var modalAdd = function() {
			$('#modalAdd').modal('show');
		}

		for (var i = 0; i < elementsAdd.length; i++) {
			elementsAdd[i].addEventListener('click', modalAdd, false);
		}

		// Datepicker
		$('#dateNaissance').datepicker({
			format : "dd/mm/yyyy",
			todayBtn : "linked",
			language : "fr",
			todayHighlight : true
		});
		
		var tabEleveCheck = [];
		$(".eleveCheck").change(function(){
			if(this.checked)
				tabEleveCheck.push($(this).val());
			else
				tabEleveCheck.splice(tabEleveCheck.indexOf($(this).val()),1);
		});
		
		$("#submitAdd").click(function(){
			$("#listEleve").val(tabEleveCheck.toString());
			$("#formAdd").submit();
		});
	</script>
</body>

</html>