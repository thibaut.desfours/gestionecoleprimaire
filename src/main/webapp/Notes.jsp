<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %><%@ page import="java.util.List" %>
<%@ page import="com.gep.beans.Cours" %>
<%@ page import="com.gep.beans.Evaluation" %>
<%@ page import="com.gep.beans.Classe" %>
<%@ page import="com.gep.beans.Eleve" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/ico" href="img/favicon.gif" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/modify.css">
<link rel="stylesheet" href="css/sidebar.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<title>Les élèves</title>
</head>

<body>
	<%@include file="navbar.jsp" %>
    <%@include file="sidebar.jsp" %>

	<div id="main">
		<h2>Notes des élèves pour l'évaluation <%Evaluation evaluation = (Evaluation) request.getAttribute("evaluation");out.println(evaluation.getIntitule()); %></h2>
		<table id="table" class="display">
			<thead>
				<tr>
					<th>ID</th>
					<th>Elève</th>
					<th>Classe</th>
					<th>Note</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<%
				ArrayList<Eleve> eleves = (ArrayList<Eleve>) request.getAttribute("eleves");
				HashMap<Integer,Float> notes = (HashMap<Integer,Float>) request.getAttribute("notes");
				for(Eleve eleve : eleves){
					out.println("<tr>");
					out.println("<td>"+eleve.getId()+"</td>");
					out.println("<td id='eleve"+eleve.getId()+"'>"+eleve.getPrenom()+" "+eleve.getNom()+"</td>");
					out.println("<td>"+eleve.getClasse().getNom()+"</td>");
					if(notes.get(eleve.getId()) < 0){
						out.println("<td></td>");
						out.println("<td><button row_id='"+eleve.getId()+"' row_note='' class=\"modifyButton btn\" type=\"button\"><img src=\"images/modify.svg\" alt=\"\"></button></td>");
					}else{
						out.println("<td>"+notes.get(eleve.getId())+"/20</td>");
						out.println("<td><button row_id='"+eleve.getId()+"' row_note='"+notes.get(eleve.getId())+"' class=\"modifyButton btn\" type=\"button\"><img src=\"images/modify.svg\" alt=\"\"></button></td>");
					}
					out.println("</tr>");
				}
				%>
			</tbody>
		</table>
		<br>
		<div id="buttons">
			<button type="button" class="btn btn-secondary">Revenir à la
				liste</button>
		</div>
	</div>

	<div id="modifyModal" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"></h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form method="post">
				<div class="modal-body">
					<label>Note</label>
					<div class="input-group mb-3">
						<input type="text" class="form-control" id="note" name="note"
							aria-describedby="basic-addon3">
						<input type="hidden" name="eleveId" id="eleveId" />
						<input type="hidden" name="evalId" <%out.print("value = '"+evaluation.getId()+"'");%>/>
					</div>
					<p>Êtes-vous sûr de vouloir modifier la note ?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Annuler</button>
					<button id="modifyButton" type="submit" class="btn">
						Modifier la note
					</button>
				</div>
				</form>
			</div>
		</div>
	</div>

	<script src="js/sidebar.js" type="text/javascript"></script>
	<script>
		$(document)
				.ready(
						function() {
							$('#table')
									.DataTable(
											{
												"language" : {
													"url" : "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
												}
											});
							$(".modifyButton").click(function(){
								$(".modal-title").text($('#eleve'+$(this).attr('row_id')).text());
								$("#eleveId").val($(this).attr('row_id'));
								$("#note").val($(this).attr('row_note'));
							});
						});

		// Modify modal
		var elements = document.getElementsByClassName("modifyButton");

		var modifyModal = function() {
			$('#modifyModal').modal('show');
		}

		for (var i = 0; i < elements.length; i++) {
			elements[i].addEventListener('click', modifyModal, false);
		}
	</script>
</body>

</html>