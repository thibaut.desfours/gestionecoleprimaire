<%@page contentType="text/html"
        pageEncoding="UTF-8"
%>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/ico" href="img/favicon.gif" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <title>Menu</title>
</head>

<body>
    <%@include file="navbar.jsp" %>
    <%@include file="sidebar.jsp" %>

    <div id="main">
        <div class="container">
            <a href="./students">
                <div class="cards">
                    <div class="coloredSquare">
                        <img src="images/Students_black.svg" alt="">
                    </div>
                    <div class="titleCard">
                        <p>Elèves</p>
                    </div>
                    <div class="numberCard">
                        <p>
                        	<% int nbEleve = (int) request.getAttribute("nbEleve");
                        		out.println(nbEleve);	
                       		%>
                       	</p>
                    </div>
                </div>
            </a>
            <a href="./teachers">
                <div class="cards">
                    <div class="coloredSquare">
                        <img src="images/Teachers_black.svg" alt="">
                    </div>
                    <div class="titleCard">
                        <p>Professeur</p>
                    </div>
                    <div class="numberCard">
                        <p>
                        	<% int nbProf = (int) request.getAttribute("nbProf");
                        		out.println(nbProf);	
                       		%>
                       	</p>
                    </div>
                </div>
            </a>
            <a href="./classes">
                <div class="cards">
                    <div class="coloredSquare">
                        <img src="images/Classes_black.svg" alt="">
                    </div>
                    <div class="titleCard">
                        <p>Classes</p>
                    </div>
                    <div class="numberCard">
                        <p>
                        	<% int nbClasse = (int) request.getAttribute("nbClasse");
                        		out.println(nbClasse);	
                       		%>
                       	</p>
                    </div>
                </div>
            </a>
            <a href="./tests">
                <div class="cards">
                    <div class="coloredSquare">
                        <img src="images/Tests_black.svg" alt="">
                    </div>
                    <div class="titleCard">
                        <p>Evaluations</p>
                    </div>
                    <div class="numberCard">
                        <p>
							<% int nbEval = (int) request.getAttribute("nbEval");
                        		out.println(nbEval);	
                       		%>
						</p>
                    </div>
                </div>
            </a>
            <a href="./absences">
                <div class="cards">
                    <div class="coloredSquare">
                        <img src="images/Absences_black.svg" alt="">
                    </div>
                    <div class="titleCard">
                        <p>Absences</p>
                    </div>
                    <div class="numberCard">
                        <p>
							<% int nbAbs = (int) request.getAttribute("nbAbs");
                        		out.println(nbAbs);	
                       		%>
						</p>
                    </div>
                </div>
            </a>
            <a href="./lessons">
                <div class="cards">
                    <div class="coloredSquare">
                        <img src="images/Lessons_black.svg" alt="">
                    </div>
                    <div class="titleCard">
                        <p>Cours</p>
                    </div>
                    <div class="numberCard">
                        <p>
							<% int nbCours = (int) request.getAttribute("nbCours");
                        		out.println(nbCours);	
                       		%>
						</p>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <script src="js/sidebar.js" type="text/javascript"></script>
</body>

</html>