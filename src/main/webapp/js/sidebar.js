var isSidebarOpened = false;

function openOrCloseNav() {
	if (isSidebarOpened) {
		document.getElementById("mySidebar").style.width = "0";
		document.getElementById("main").style.marginRight = "0";
		isSidebarOpened = false;
	} else {
		document.getElementById("mySidebar").style.width = "90px";
		document.getElementById("main").style.marginRight = "90px";
		isSidebarOpened = true;
	}
}