<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.gep.beans.Prof" %>
<%@ page import="com.gep.beans.Classe" %>
<%@ page import="com.gep.beans.Niveau" %>
<%@ page import="com.gep.beans.Eleve" %>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/ico" href="img/favicon.gif" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/add.css">
<link rel="stylesheet" href="css/sidebar.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<title>Ajout d'une classe</title>
</head>

<body>
	<%@include file="navbar.jsp" %>
    <%@include file="sidebar.jsp" %>
    
	<div id="main">
		<form method="post">
		<h2>Ajouter une classe</h2>
		<label>Nom</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="nom" name="nom"
				aria-describedby="basic-addon3">
		</div>
		<label>Annee</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="annee" name="annee"
				aria-describedby="basic-addon3">
		</div>
		<label>Niveau</label><br>
		<select class="form-control" name="selectNiveau">
			<%
				ArrayList<Niveau> niveaux = (ArrayList<Niveau>) request.getAttribute("niveaux");
				out.println("<option selected disabled>Veuillez sélectionner un niveau</option>");
				for(Niveau niveau : niveaux){
					out.println("<option value='"+niveau.getId()+"'>"+niveau.getLibelle()+"</option>");
				}
			%>
		</select>
		<label class="label">Professeur</label><br>
		<select class="form-control" name="selectProf">
				<%
					ArrayList<Prof> profs = (ArrayList<Prof>) request.getAttribute("profs");
					out.println("<option selected disabled>Veuillez sélectionner un professeur</option>");
					for(Prof prof : profs){
						out.println("<option value='"+prof.getId()+"'>"+prof.getPrenom()+" "+prof.getNom()+"</option>");
					}
				%>
			</select>
		<br>
		
		<br>
		<div id="buttons">
			<button type="button" class="btn btn-secondary">Annuler et
				revenir à la liste</button>
			<button id="addButton" type="submit" class="btn">
				<img src="images/Plus.svg" alt="">Ajouter
			</button>
		</div>
		</form>
	</div>

	<script src="js/sidebar.js" type="text/javascript"></script>
</body>

</html>