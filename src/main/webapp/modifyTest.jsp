<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.gep.beans.Evaluation" %>
<%@ page import="com.gep.beans.Cours" %>
<%@ page import="com.gep.beans.Classe" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" type="image/ico" href="img/favicon.gif" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/modify.css">
<link rel="stylesheet" href="css/sidebar.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<title>Modification d'une évaluation</title>
</head>
<body>
	<%@include file="navbar.jsp" %>
    <%@include file="sidebar.jsp" %>
	<div id="main">
		<form method="post">
		<input type="hidden" name="id" <%Evaluation eval = (Evaluation) request.getAttribute("evaluation");out.println("value ='"+eval.getId()+"'");%>/>
		<h2>Modification de l'évaluation <%out.println(eval.getIntitule()); %></h2>
		<label>Intitulé</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="intitule" name="intitule" <% out.println("value ='"+eval.getIntitule()+"'"); %>
				aria-describedby="basic-addon3">
		</div>
		<label>Cours</label><br>
		<select class="form-control" name="selectCours">
			<%				
				ArrayList<Cours> coursL = (ArrayList<Cours>) request.getAttribute("cours");
				for(Cours cours : coursL){
					if(eval.getCours().getId() == cours.getId())
						out.println("<option selected value='"+cours.getId()+"'>"+cours.getClasse().getNom()+" : "+cours.getIntitule()+"</option>");
					else
						out.println("<option value='"+cours.getId()+"'>"+cours.getClasse().getNom()+" : "+cours.getIntitule()+"</option>");
				}
			%>
		</select>
		<br>
		<div id="buttons">
			<button type="button" class="btn btn-secondary">Annuler et
				revenir à la liste</button>
			<button id="modifyButton" type="submit" class="btn">
				<img src="images/pencil.svg" alt="">Accepter les modifications
			</button>
		</div>
		</form>
	</div>
	<script src="js/sidebar.js" type="text/javascript"></script>
</body>
</html>