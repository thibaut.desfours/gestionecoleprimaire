<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %><%@ page import="java.util.List" %>
<%@ page import="com.gep.beans.Classe" %>
<%@ page import="com.gep.beans.Eleve" %>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/ico" href="img/favicon.gif" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/modify.css">
<link rel="stylesheet" href="css/sidebar.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<title>Modification d'un élève</title>
</head>

<body>
	<%@include file="navbar.jsp" %>
    <%@include file="sidebar.jsp" %>

	<div id="main">
		<h2>Modification de l'élève <% Eleve eleve = (Eleve) request.getAttribute("eleve"); out.println(eleve.getPrenom()+" "+eleve.getNom()); %></h2>
		<form method="post" >
			<input type="hidden" name="id" value="<% out.println(eleve.getId()); %>">
			<label>Nom</label>
			<div class="input-group mb-3">
				<input type="text" class="form-control" id="nom" name="nom" value="<% out.println(eleve.getNom()); %>"
					aria-describedby="basic-addon3">
			</div>
			<label>Prénom</label>
			<div class="input-group mb-3">
				<input type="text" class="form-control" id="prenom" name="prenom" value="<% out.println(eleve.getPrenom()); %>"
					aria-describedby="basic-addon3">
			</div>
			<label>Date de naissance</label> <input id="dateNaissance" type="text" name="dateNaissance" value="<% out.println(eleve.getDateN()); %>"
				class="form-control"> <label id="labelClass">Classe</label><br>
			<select class="form-control" name="selectClasse">
				<%
					ArrayList<Classe> classes = (ArrayList<Classe>) request.getAttribute("classes");
					for(Classe classe : classes){
						if(eleve.getClasse().getId() == classe.getId())
							out.println("<option selected value='"+classe.getId()+"'>"+classe.getNom()+"</option>");
						else
							out.println("<option value='"+classe.getId()+"'>"+classe.getNom()+"</option>");
					}
				%>
			</select>
			<br> <label>Téléphone des parents</label>
			<div class="input-group mb-3">
				<input type="text" class="form-control" id="telephoneParents" name="telephoneParents" value="<% out.println(eleve.getTel()); %>"
					aria-describedby="basic-addon3">
			</div>
			<label>Mail des parents</label>
			<div class="input-group mb-3">
				<input type="text" class="form-control" id="mailParents" name="mailParents" value="<% out.println(eleve.getMail()); %>"
					aria-describedby="basic-addon3">
			</div>
			<label>Adresse</label>
			<div class="input-group mb-3">
				<input type="text" class="form-control" id="adresse" name="adresse" value="<% out.println(eleve.getAdresse()); %>"
					aria-describedby="basic-addon3">
			</div>
			<label>Ville</label>
			<div class="input-group mb-3">
				<input type="text" class="form-control" id="ville" name="ville" value="<% out.println(eleve.getVille()); %>"
					aria-describedby="basic-addon3">
			</div>
			<div id="buttons">
				<button type="button" class="btn btn-secondary">
					Annuler et revenir à la liste
				</button>
				<button id="modifyButton" type="submit" class="btn">
					<img src="images/pencil.svg" alt="">Accepter les modifications
				</button>
			</div>
		</form>
	</div>

	<script src="js/sidebar.js" type="text/javascript"></script>
	<script>
		// Datepicker
		$('#dateNaissance').datepicker({
			format : "yyyy-mm-dd",
			todayBtn : "linked",
			language : "fr",
			todayHighlight : true
		});
	</script>
</body>

</html>